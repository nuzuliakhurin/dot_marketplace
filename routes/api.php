<?php

use Illuminate\Http\Request;

use App\Http\Resources\ProductsCollection;
use App\Products;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();

});





Route::post('/register', 'API\auth\RegisterController@register');

Route::group(['middleware'=>'auth:api'], function(){
	Route::get('/user/detail', 'API\V1\UserController@detail');

	Route::prefix('v1')->group(function(){

	// Route::prefix('belajar')->group(function(){

	// 	Route::get('cobaget', 'API\V1\CobaController@method_get');
	// 	Route::post('cobapost', 'API\V1\CobaController@method_post');
	// 	Route::post('cuoba', 'API\V1\CobaController@method_cuoba');
	// 	Route::get('sapa', 'CobaController@sapa');
	// });

	// Route::prefix('kategori')->group(function(){
	// 	Route::get('/', 'API\V1\CategoryController@index');
	// 	Route::get('/{id}/detail', 'API\V1\CategoryController@detail');
	// 	Route::post('/add', 'API\V1\CategoryController@store');

	// });

	// Route::prefix('customers')->group(function(){
	// 	Route::get('/', 'API\V1\CustomersController@index');
	// 	Route::get('/{id}/detail', 'API\V1\CustomersController@detail');
	// 	Route::post('/add', 'API\V1\CustomersController@store');

	// });

		Route::apiResource('category', 'API\V1\CategoryController');
		Route::apiResource('customers', 'API\V1\CustomersController');
		Route::apiResource('products', 'API\V1\ProductsController');

		Route::prefix('order')->group(function(){
			Route::post('/store', 'API\V1\OrderController@store');
			Route::get('/{id}', 'API\V1\OrderController@show');
		});

		Route::prefix('order_detail')->group(function(){
			Route::post('/add', 'API\V1\OrderDetailController@store');
		});

	// Route::get('/products/{id}', function(Products $products){
	// 	return new ProductsResource($products);
	// });

	//route resource
	// Route::get('/products', function(){
	// 	return apiResponseBulider(200, ProductsResource::collection(Products::all()));
	// });

	// Route::get('/products/{id}', function(Products $id){
	// 	return apiResponseBulider(200, new ProductsResource($id));
	// });



	// Route::get('/products', function() {
	// 	return new ProductsCollection(Products::all());
	// });	
	});

});
Route::post('/login', 'API\auth\LoginController@login');


