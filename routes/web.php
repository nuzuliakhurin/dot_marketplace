<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
* ROUTE LOGIN
*/

Route::get('/login', 'LoginController@showLogin')->name('login'); // tampilan login
Route::post('/login', 'LoginController@doLogin')->name('login.doLogin'); // proses login

Route::get('/logout', 'LoginController@logout')->name('logout');

Route::middleware("auth")->group(function() {
	/**
* ROUTE KATEGORI
*/
Route::get('/', function () {
	return view('template', ['kategori'=>[]]);
});

Route::get('/home', function(){
	return "Haiii";
});

Route::get('/home/{nama}', function($nama){
	return "Haloo selamat siang $nama";
});

Route::get('/home/{nama}/{sapa}', function($nama,$sapa){
	return "Haii $nama $sapa";
});

Route::get('/uhuy', 'CobaController@homec');
Route::get('/uhuy/{nama}', 'CobaController@homeNama');


//ROUTE KATEGORI
Route::prefix('category')->group(function(){

Route::get('/', 'CategoryController@index');
// Route::post('/category', 'CategoryController@submit');
Route::get('/{id}', 'CategoryController@detail');
Route::get('/edit/{id}', 'CategoryController@edit');
Route::post('/update/{id}', 'CategoryController@update');
Route::post('/store', 'CategoryController@store');
Route::get('/delete/{id}', 'CategoryController@delete');
Route::resource("category", "CategoryController")->except([
		"create"]);
});


// Route::get('/products', function(){
// 	return view('product');
// });

// ROUTE PRODUCT

Route::prefix('products')->group(function(){

	Route::get('/', 'ProductsController@index');
	Route::get('/{id}', 'ProductsController@detail');
	Route::get('/edit/{id}', 'ProductsController@edit');
	Route::post('/update/{id}', 'ProductsController@update');
	Route::post('/store', 'ProductsController@store');
	Route::get('/delete/{id}', 'ProductsController@delete');
});


// ROUTE CUSTOMERS

Route::prefix('customers')->group(function(){

	Route::get('/', 'CustomersController@index');
	Route::get('/{id}', 'CustomersController@detail');
	Route::post('/store', 'CustomersController@store');
	Route::get('/edit/{id}', 'CustomersController@edit');
	Route::get('/delete/{id}', 'CustomersController@delete');
	Route::post('/update/{id}', 'CustomersController@update');
});

// ROUTE ORDER
Route::prefix('orders')->group(function(){

	Route::get('/', 'OrdersController@index');
	Route::post('/store', 'OrdersController@store');
	Route::get('/{id}', 'OrdersController@detail');
	Route::post('/{id}/add', 'OrdersController@addProducts')->name('order.add');
	Route::get('/delete/{id}', 'OrdersController@delete');
});


});
