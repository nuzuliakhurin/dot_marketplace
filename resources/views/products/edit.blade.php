@include('base.header')
@include('base.menu')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Edit Product
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					
					<form role="form" action="/products/update/{{ $dataProducts->id }}" method="post" enctype="multipart/form-data">
						@csrf 
						<!-- /.box-header -->
						<div class="box-body">
							@if (Session::has('massage'))
							<div class="alert alert-success">{{Session::get('massage')}}</div>
							@endif

							@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
							@endif

							<table class="table table-bordered">
								<tr>
									<td>Nama Product</td>
									<td><input type="text" name="name" value="{{$dataProducts->name}}"></td>
								</tr>
								<tr>
									<td>Kategori</td>
									<td>
										<select name="category_id">
											<option>-- Pilih Salah Satu --</option>
											@foreach($dataCategory as $kategori)
											<option value="{{ $kategori->id }}" {{(  $dataProducts->category_id == $kategori->id) ? 'selected' :''}}>{{$kategori->category_name}}</option>
											@endforeach
										</select>
									</td>
								</tr>
								<tr>
									<td>Harga Per Item</td>
									<td><input type="number" name="unit_price" value="{{$dataProducts->unit_price}}"></td>
								</tr>
								<tr>
									<td>Foto Produk</td>
									<td>
										<img src="/images/{{ $dataProducts->image }}" width="100px">
										<input type="file" name="image"></td>
								</tr>

							</table>
							<br>
							<input type="submit" class="btn btn-primary" value="Update">
							<a href="/products" class="btn btn-warning">Back</a>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>

@include('base.footer')