@include('base.header')
@include('base.menu')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Detail Product
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<td>ID</td>
								<td>{{$dataProducts->id}}</td>
							</tr>
							<tr>
								<td>Nama Product</td>
								<td>{{$dataProducts->name}}</td>
							</tr>
							<tr>
								<td>Kategori</td>
								<td>{{$dataProducts->category->category_name}}</td>
							</tr>
							<tr>
								<td>Harga Per Item</td>
								<td>{{$dataProducts->unit_price}}</td>
							</tr>
							<tr>
								<td>Gambar Product</td>
								<td><img src="/images/{{ $dataProducts->image }}" width="100px"></td>
							</tr>
							<tr>
								<td><a href="/products" class="btn btn-warning">Back</a></td>
							</tr>
							
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>

@include('base.footer')