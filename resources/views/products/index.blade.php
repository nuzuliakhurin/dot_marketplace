@include('base.header')
@include('base.menu')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Product
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Tambah Product</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form role="form" action="/products/store" method="post" enctype="multipart/form-data">
						@csrf 
						<!-- {{ csrf_field() }} -->
						<div class="box-body">
							@if (Session::has('massage'))
							<div class="alert alert-success">{{Session::get('massage')}}</div>
							@endif

							@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
							@endif
							<div class="form-group">
								<label for="exampleInputEmail1">Nama Product</label>
								<input type="text" name="name" class="form-control" placeholder="Masukkan nama produk">

								<label for="exampleInputEmail1">Kategori Product</label>
								<select class="form-control" name="category_id" required>
									<option>-- Pilih Salah Satu --</option>
									@foreach($category as $kategori)
										<option value="{{ $kategori->id }}">{{$kategori->category_name}}</option>
									@endforeach
								</select>

								<label for="exampleInputEmail1">Harga Satuan</label>
								<input type="number" name="unit_price" class="form-control" placeholder="Masukkan harga satuan produk">

								<label for="exampleInputEmail1">Gambar Produk</label>
								<input type="file" name="image" >
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<button type="submit" class="btn btn-primary">Tambah</button>
						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Daftar Product</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<form action="/products" method="GET">
							<span class="pull-right">
								<input type="text" name="search" class="form-control" placeholder="Search Here ...">
							</span>
						</form>
						<table class="table table-bordered">
							<tr>
								<th>No</th>
								<th>Nama Product</th>
								<th>Kategori</th>
								<th>Harga</th>
								<th>Gambar Produk</th>
								<th>Aksi</th>
							</tr>
							<?php $no=0;?>
							@foreach($dataProducts as $item)
							<?php $no++; ?>
							<tr>
								<td>{{ $no }}</td>
								<td>{{ $item->name }}</td>
								<td>{{ $item->category->category_name }}</td>
								<td>{{ $item->unit_price }}</td>
								<td><img src="/images/{{ $item->image }}" width="50px"></td>
								<td>
									<a href="/products/{{$item->id}}" class="btn btn-success">Detail</a>
									<a href="/products/edit/{{$item->id}}" class="btn btn-primary">Edit</a>
									<a href="/products/delete/{{$item->id}}" class="btn btn-danger">Hapus</a>
								</td>
							</tr>
							@endforeach
						</table>
					</div>

					<div class="text-center">
						{!! $dataProducts->appends(request()->all())->links() !!}
					</div>
				</div>
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>

	@include('base.footer')