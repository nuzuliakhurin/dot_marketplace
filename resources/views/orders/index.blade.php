@include('base.header')
@include('base.menu')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Order
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Order Product</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form role="form" action="/orders/store" method="post">
						@csrf 
						<!-- {{ csrf_field() }} -->
						<div class="box-body">
							@if (Session::has('massage'))
							<div class="alert alert-success">{{Session::get('massage')}}</div>
							@endif

							@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
							@endif
							<div class="form-group">
								<label for="exampleInputEmail1">Nama Customers</label>
								<select name="customer_id" class="form-control">
									<option>-- Pilih Nama Customer --</option>
									@foreach($dataCustomers as $cust)
										<option value="{{ $cust->id }}">{{ $cust->first_name }} {{ $cust->last_name }}</option>
									@endforeach
								</select>

							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<button type="submit" class="btn btn-primary">Tambah</button>
						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Daftar Porduct</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<th>ID Order</th>
								<th>Nama Customer</th>
								<th>Total</th>
								<th>Aksi</th>
							</tr>
							
							@foreach($dataOrders as $order)
							
							<tr>
								<td>{{ $order->id }}</td>
								<td>{{ $order->customers->first_name }} {{ $order->customers->last_name}}</td>
								<td>{{ $order->total }}</td>
								<td>
									<a href="/orders/{{$order->id}}" class="btn btn-success">Detail</a>
									<a href="/orders/delete/{{$order->id}}" class="btn btn-danger">Hapus</a>
								</td>
							</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>

	@include('base.footer')