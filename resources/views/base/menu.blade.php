<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>
        <li>
          <a href="/customers">
            <i class="fa fa-circle-o"></i> <span>Customer</span>
          </a>
          <a href="/products">
            <i class="fa fa-circle-o"></i> <span>Produk</span>
          </a>
          <a href="/category">
            <i class="fa fa-circle-o"></i> <span>Kategori</span>
          </a>
          <a href="/orders">
            <i class="fa fa-circle-o"></i> <span>Order</span>
          </a>
          <a href="{{ route('logout')}}">
            <i class="fa fa-log-out"></i> <span>Logout</span>
          </a>
        </li>
        <li>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>