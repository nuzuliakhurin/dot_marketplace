@include('base.header')
@include('base.menu')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Kategori
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Tambah Kategori</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
					<form role="form" action="/category/store" method="post">
						@csrf 
						<!-- {{ csrf_field() }} -->
						<div class="box-body">
							@if (Session::has('massage'))
							<div class="alert alert-success">{{Session::get('massage')}}</div>
							@endif

							@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
							@endif

							<div class="form-group">
								<label for="exampleInputEmail1">Nama Kategori</label>
								<input type="text" name="category_name" class="form-control" placeholder="Masukkan nama kategori">
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<button type="submit" class="btn btn-primary">Tambah</button>
						</div>
					</form>
				</div>
				<!-- /.box -->
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Daftar Kategori</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<form action="/category" method="GET">
							<span class="pull-right">
								<input type="text" name="search" class="form-control" placeholder="Search Here ...">
							</span>
						</form>
						<table class="table table-bordered">
							<tr>
								<th>No</th>
								<th>Nama Kategori</th>
								<th>Jumlah Produk</th>
								<th>Aksi</th>
							</tr>
							<?php $no=0;?>
							@foreach($dataCategory as $kategori)
							<?php $no++; ?>
							<tr>
								<td>{{ $no }}</td>
								<td>{{ $kategori->category_name }}</td>
								<td>{{ $kategori->product_count }}</td>
								<td>
									<a href="/category/{{$kategori->id}}" class="btn btn-success">Detail</a>
									<a href="/category/edit/{{$kategori->id}}" class="btn btn-primary">Edit</a>
									<a href="/category/delete/{{$kategori->id}}" class="btn btn-danger">Hapus</a>
								</td>
							</tr>
							@endforeach
						</table>
					</div>

					<div class="text-center">
						{!! $dataCategory->appends(request()->all())->links() !!}
					</div>
				</div>
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>

	@include('base.footer')