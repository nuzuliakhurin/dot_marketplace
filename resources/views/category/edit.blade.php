@include('base.header')
@include('base.menu')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Edit Kategori
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					
					<form role="form" action="/category/update/{{ $dataCategory->id }}" method="post">
						@csrf 
						<!-- /.box-header -->
						<div class="box-body">
							@if (Session::has('massage'))
							<div class="alert alert-success">{{Session::get('massage')}}</div>
							@endif

							@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
							@endif

							<table class="table table-bordered">
								<tr>
									<td>Nama Product</td>
									<td><input type="text" name="category_name" value="{{$dataCategory->category_name}}"></td>
								</tr>
								

							</table>
							<br>
							<input type="submit" class="btn btn-primary" value="Update">
							<a href="/category" class="btn btn-warning">Back</a>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>

@include('base.footer')