@include('base.header')
@include('base.menu')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Kategori {{$dataCategory->category_name}}
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<thead>
								<tr>
									<td>ID</td>
									<td>Nama Product</td>
								</tr>
							</thead>
							<tbody>
								@forelse($dataCategory->products as $products)
									<tr>
										<td>{{$products->id}}</td>
										<td>{{$products->name}}</td>
									</tr>
								@empty
									<tr>
										<td colspan="2" align="center">Belum ada produk dalam kategori ini.</td>
									</tr>
								@endforelse
							</tbody>
							<tr>
								<td><a href="/category" class="btn btn-warning">Back</a></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>

@include('base.footer')