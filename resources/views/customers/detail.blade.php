@include('base.header')
@include('base.menu')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Detail Customers
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table table-bordered">
							<tr>
								<td>ID</td>
								<td>{{$dataCustomers->id}}</td>
							</tr>
							<tr>
								<td>Nama Depan</td>
								<td>{{$dataCustomers->first_name}}</td>
							</tr>
							<tr>
								<td>Nama Belakang</td>
								<td>{{$dataCustomers->last_name}}</td>
							</tr>
							<tr>
								<td>Email</td>
								<td>{{$dataCustomers->email}}</td>
							</tr>
							<tr>
								<td>Password</td>
								<td>{{$dataCustomers->password}}</td>
							</tr>
							<tr>
								<td>Alamat</td>
								<td>{{$dataCustomers->address}}</td>
							</tr>
							<tr>
								<td>Nomor HP</td>
								<td>{{$dataCustomers->phone_number}}</td>
							</tr>
							
							<tr>
								<td><a href="/customers" class="btn btn-warning">Back</a></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>

@include('base.footer')