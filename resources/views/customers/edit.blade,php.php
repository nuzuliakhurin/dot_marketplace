@include('base.header')
@include('base.menu')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Detail Product
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					
					<form role="form" action="/customers/update/{{ $dataCustomers->id }}" method="post">
						@csrf 
						<!-- /.box-header -->
						<div class="box-body">
							@if (Session::has('massage'))
							<div class="alert alert-success">{{Session::get('massage')}}</div>
							@endif

							@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
							@endif
							
							<table class="table table-bordered">
								<tr>
									<td>Nama Depan</td>
									<td><input type="text" name="first_name" value="{{$dataCustomers->first_name}}"></td>
								</tr>
								<tr>
									<td>Nama Belakang</td>
									<td><input type="text" name="last_name" value="{{$dataCustomers->last_name}}"></td>
								</tr>
								<tr>
									<td>Email</td>
									<td><input type="email" name="email" value="{{$dataCustomers->email}}"></td>
								</tr>
								<tr>
									<td>Password</td>
									<td><input type="password" name="password" value="{{$dataCustomers->password}}"></td>
								</tr>
								<tr>
									<td>Alamat</td>
									<td><textarea name="address">{{$dataCustomers->address}}</textarea></td>
								</tr>
								<tr>
									<td>Nomor HP</td>
									<td><input type="number" name="phone_number" value="{{$dataCustomers->phone_number}}"></td>
								</tr>

							</table>
							<br>
							<input type="submit" class="btn btn-primary" value="Update">
							<a href="/customers" class="btn btn-warning">Back</a>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>

@include('base.footer')