<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    protected $table = 'order_details';
    use SoftDeletes;

    protected $fillable = ['order_id', 'product_id', 'quantity', 'price'];

    public function Products()
    {
    	return $this->hasOne(Products::class, 'id', 'product_id');
    }


}
