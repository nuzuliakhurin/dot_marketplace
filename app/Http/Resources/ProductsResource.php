<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       $dataProducts = [
        'Nama' => $this->name,
        'Harga' => $this->unit_price,
        'Gambar'  => asset('/images/'.$this->image),
        'ID' => $this->id,
        'Kategori' => [
            'ID Kategori' => $this->category_id,
            'Nama Kategori' => $this->category->category_name
        ]
    ];

    return $dataProducts;
    }
}
