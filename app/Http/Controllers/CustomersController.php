<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customers;

class CustomersController extends Controller
{
	public function index()
	{
		$dataCustomers = Customers::query();

		if (request()->has('search') && strlen(request()->query('search')) >= 1) {
			$dataCustomers->where(
				'first_name', 'like', '%' . request()->query('search').'%'
			)->orWhere('last_name', 'like', '%' . request()->query('search').'%');
		}

		$pagination = 3;
		$dataCustomers = $dataCustomers->orderby('created_at', 'desc')
		->paginate($pagination);

		$number = 1;
		if (request()->has('page') && request()->get('page')>1) {
			$number += (request()->get('page')-1) * $pagination;
		}

		$customers = Customers::select(['id', 'first_name'])->orderby('first_name')->get();

		return view('customers.index', compact('dataCustomers', 'customers'));
	}

	public function detail($id)
	{
		$dataCustomers = Customers::find($id);
		return view('customers.detail', compact('dataCustomers'));
	}

	// ke halaman edit
	public function edit($id)
	{
		$dataCustomers = Customers::find($id);
		return view('customers.edit', compact('dataCustomers'));
	}

	// TAMBAH DATA
	public function store(Request $request)
	{
		$this->validate($request, [
			'first_name' => 'required | min:3',
			'last_name' => 'required | min:3',
			'email' => 'required',
			'password' => 'required',
			'address' => 'required',
			'phone_number' => 'required',
		]);

		$dataCustomers = new Customers;

		$dataCustomers->first_name = $request->first_name;
		$dataCustomers->last_name = $request->last_name;
		$dataCustomers->email = $request->email;
		$dataCustomers->password = $request->password;
		$dataCustomers->address = $request->address;
		$dataCustomers->phone_number = $request->phone_number;


		$dataCustomers->save();

		if ($dataCustomers) {
			$request->session()->flash('massage', 'Berhasil Tambah Customers!');
		}

		return redirect()->back();
	}

	public function update(Request $request, $id)
	{

		$this->validate($request, [
			'first_name' => 'required | min:3',
			'last_name' => 'required | min:3',
			'email' => 'required',
			'password' => 'required',
			'address' => 'required',
			'phone_number' => 'required',
		]);
		
		$dataCustomers = Customers::find($id);

		$dataCustomers->first_name = $request->first_name;
		$dataCustomers->last_name = $request->last_name;
		$dataCustomers->email = $request->email;
		$dataCustomers->password = $request->password;
		$dataCustomers->address = $request->address;
		$dataCustomers->phone_number = $request->phone_number;

		$dataCustomers->save();

		if ($dataCustomers) {
			$request->session()->flash('massage', 'Berhasil Update Customers!');
		}

		return redirect()->back();
	}

	public function delete($id)
	{
		$dataCustomers = Customers::find($id);
		
		if ($dataCustomers != null) {
			$dataCustomers->delete();
			return redirect()->back()->with(['message'=> 'Successfully deleted!!']);
		}

		
	}

}
