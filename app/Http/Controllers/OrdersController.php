<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderDetail;
use App\Orders;
use App\Customers;
use App\Products;

class OrdersController extends Controller
{
    public function index()
    {
    	$dataOrders = Orders::all();
    	$dataCustomers = Customers::all();
      return view('orders.index', compact('dataOrders', 'dataCustomers'));
  }

  public function store(Request $request)
  {
    	// dd($request->all()); cek data
   $request->validate([
    'customer_id'   => 'required',
]);


   $dataOrders = new Orders;
   $dataOrders->customer_id = $request->customer_id;
   $dataOrders->total = 0;
   $dataOrders->save();

   if ($dataOrders) {
      $request->session()->flash('massage', 'Berhasil Tambah Customer!');
  }

  return redirect()->back()->with('success', 'Berhasil Tambah');
}

public function detail($id)
{
    $dataProducts = Products::all();
    $dataOrderDetail = OrderDetail::where('order_id', $id)->get();
    $total = Orders::select('total')->where('id', $id)->first();
    $detailId = $id;
    return view('orders.detail', compact('dataProducts', 'detailId', 'dataOrderDetail', 'total'));
}

public function addProducts(Request $request, $id)
{
    $request->validate([
        'product_id' => 'required',
        'quantity' => 'required',
    ]);

        //ambil data order dengan id produk
    $checkProduct = OrderDetail::where('product_id', $request->product_id)->where('order_id', $id)->first();

        //ambil data harga produk
    $dataProducts = Products::where('id', $request->product_id)->first();

        //cek produk di detail ada atau tidak
    if ($checkProduct) {
        $checkProduct->order_id = $checkProduct->order_id;
        $checkProduct->quantity = $checkProduct->quantity + $request->quantity;
        $checkProduct->price = $checkProduct->price + ($request->quantity * $dataProducts->unit_price);
        $checkProduct->save();
    } else {
        $dataOrderDetail = new OrderDetail;
        $dataOrderDetail->order_id      = $id;
        $dataOrderDetail->product_id    = $request->product_id;
        $dataOrderDetail->quantity      = $request->quantity;
        $dataOrderDetail->price         = $request->quantity * $dataProducts->unit_price;
        $dataOrderDetail->save();
    }

    $getOrder = OrderDetail::where('order_id', $id)->get();

    $total = $getOrder->sum('price');

    $dataOrders = Orders::find($id);
    $dataOrders->total = $total;
    $dataOrders->save();

    return redirect()->back();

}

public function delete($id)
{
    $dataOrders = Orders::find($id);
    $dataOrders->delete();

    return redirect()->back();
}
}
