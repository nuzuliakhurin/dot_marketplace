<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
    * Tampilkan Form Login
    */
    public function showLogin()
    {
    	if (Auth::user()) {
    		return redirect('products');
    	} else {
    		return view('auth.login');
    	}
    }

    /**
    * Proses Login
    */
    public function doLogin(Request $request)
    {
    	//Ambil data user kecuali token
    	$credential = $request->except('_token');

    	//Attempt to login
    	if (Auth::attempt($credential)) {
    		return redirect('/products');
    	} else {
    		return redirect()->back();
    	}
    }

    // Logout
    public function logout()
    {
    	Auth::logout();
    	return redirect()->route('login');
    }
}
