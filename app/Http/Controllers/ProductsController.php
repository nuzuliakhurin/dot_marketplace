<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Products;
use App\Category;
use Session;
use Exception;

/**
 * 
 */
class ProductsController extends Controller
{
	public function index()
	{
		//  $dataProducts = DB::table('products')->get();
		// return view ('product', ['products'=>$products]);

		//Search
		$dataProducts = Products::query(); //buat query kosongan
		if (request()->has('search') && strlen(request()->query('search')) >= 1) {
			$dataProducts->where(
				'name', 'like', '%' . request()->query('search').'%'
			);
		}


		//pagination
		$pagination = 10;
		$dataProducts = $dataProducts->orderby('created_at', 'desc')
		->paginate($pagination);

		$number = 1;
		if (request()->has('page') && request()->get('page')>1) {
			$number += (request()->get('page')-1) * $pagination;
		}

		$category = Category::select(['id', 'category_name'])->orderby('category_name')->get();

		return view('products.index', compact('dataProducts', 'number', 'category'));
	}

	public function detail($id)
	{
		$dataProducts = Products::find($id);
		return view('products.detail', compact('dataProducts'));
	}

	// ke halaman edit
	public function edit($id)
	{
		$dataCategory = Category::select(['id', 'category_name'])->orderby('category_name')->get(); //get  kategori
		$dataProducts = Products::find($id);
		return view('products.edit', compact('dataProducts', 'dataCategory'));
	}

	// TAMBAH DATA
	public function store(Request $request)
	{

		$this->validate($request, [
			'name' => 'required | min:3',
			'category_id' => 'required',
			'unit_price' => 'required| numeric',
			'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);

		try {
			\DB::beginTransaction();

			Category::where('id', $request->category_id)->increment('product_count');
			$imageName = time().'.'.request()->image->getClientOriginalExtension();
			request()->image->move(public_path('images'), $imageName);

			$dataProducts = new Products();

			$dataProducts->name = $request->name;
			$dataProducts->category_id = $request->category_id;
			$dataProducts->unit_price = $request->unit_price;
			$dataProducts->image = $imageName;
			
			$dataProducts->save();

			\DB::commit();

			$request->session()->flash('massage', 'Berhasil Tambah!');
			return redirect()->back();
		} catch (Exception $e){
			\DB::rollBack();
			// report($e);
			$request->session()->flash('massage', 'Gagal Tambah!');
			return redirect()->back();
		}

		return redirect()->back();


	// $this->validate($request, [
	// 	'name' => 'required | min:3',
	// 	'category_id' => 'required',
	// 	'unit_price' => 'required | numeric',
	// ]);

	// $dataProducts = new Products;

	// $dataProducts->name = $request->name;
	// $dataProducts->category_id = $request->category_id;
	// $dataProducts->unit_price = $request->unit_price;

	// $dataProducts->save();

	// if ($dataProducts) {
	// 	$request->session()->flash('massage', 'Berhasil Tambah Produk!');
	// }

		// return redirect()->back();
	}

	public function update(Request $request, $id)
	{

		$this->validate($request, [
			'name' => 'required | min:3',
			'category_id' => 'required',
			'unit_price' => 'required | numeric',
		]);

		$dataProducts = Products::find($id);

		$dataProducts->name = $request->name;
		$dataProducts->category_id = $request->category_id;
		$dataProducts->unit_price = $request->unit_price;

		if($request->image) {
			
			$imageName = time().'.'.request()->image->getClientOriginalExtension();
			request()->image->move(public_path('/images'), $imageName);

            $dataProducts->image = $imageName;
		}

		$dataProducts->save();
		// if ($request->user()->image) {
		// 		Storage::delete($request->user()->image);
		// 	}
		// 	$image= $request->file('image')->store('images');
		// 	$request->user()->update([
		// 		'image'->$image
		// 	]);

		if ($dataProducts) {
			$request->session()->flash('massage', 'Berhasil Update Produk!');
		}
		

		return redirect()->back();
	}

	public function delete($id)
	{
		$dataProducts = Products::withTrashed()->find($id);
		if (!$dataProducts->trashed()) {
			$dataProducts->delete();
		}else{
			$dataProducts->forceDelete();
		}
		return redirect()->back()->with(['message'=> 'Successfully deleted!!']);
	}
}
?>