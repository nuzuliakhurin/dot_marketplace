<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class KategoriController extends Controller
{
	public function index()
	{
		$kategori = Category::all();
		return view('template', compact('kategori'));
	}
	// function submit(Request $request)
	// {
	// 	// var_dump($request->kategori);
	// 	// $data = $request->all();
	// 	// $request->session()->get('kategori');
	// 	// return view('template', ['kategori' => $data]);

	// 	if (!Session::get('kategori')) {
	// 		Session::put('kategori',
	// 			array(
	// 				'1' => $request-> kategori
	// 			)
	// 		);
	// 	} else {
	// 		Session::push('kategori',
	// 			$request->kategori
	// 		);
	// 	}
	// 	return redirect('/');

	// 	// $kategori = $request->input('nama_kategori');
	// 	// $kategori_all = json_decode($request->kategori,TRUE);
	// 	// $kategori_all = array_merge($kategori, $kategori_all);
	// 	// return view('template', ['data' => kategori_all]);
	// }

	function delete(Request $request){
		$request->session()->forget('kategori');
		return redirect('/');
	}
}
?>