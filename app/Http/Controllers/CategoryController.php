<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index()
    {
    	//Search
		$dataCategory = Category::query(); //buat query kosongan
		if (request()->has('search') && strlen(request()->query('search')) >= 1) {
			$dataCategory->where(
				'category_name', 'like', '%' . request()->query('search').'%'
			);
		}


		//pagination
		$pagination = 5;
		$dataCategory = $dataCategory->orderby('created_at', 'desc')
									->paginate($pagination);
									//tambahi ->withTrashed() untuk menampilkan data yg udah dihapus
		$number = 1;
		if (request()->has('page') && request()->get('page')>1) {
			$number += (request()->get('page')-1) * $pagination;
		}

		return view('category.index', compact('dataCategory', 'number'));
    }

    public function detail($id)
    {
    	$dataCategory = Category::with('products')->find($id);
		return view('category.detail', compact('dataCategory'));
    }

    public function edit($id)
    {
    	$dataCategory = Category::find($id);
		return view('category.edit', compact('dataCategory'));
    }

    public function store(Request $request)
	{

		$this->validate($request, [
			'category_name' => 'required',
		]);

		$dataCategory = new Category;

		$dataCategory->category_name = $request->category_name;
		$dataCategory->product_count = 0;

		$dataCategory->save();

		if ($dataCategory) {
			$request->session()->flash('massage', 'Berhasil Tambah Kategori!');
		}

		return redirect()->back();
	}

	public function update(Request $request, $id)
	{

		$this->validate($request, [
			'category_name' => 'required | min:3',
		]);

		$dataCategory = Category::find($id);

		$dataCategory->category_name = $request->category_name;

		$dataCategory->save();

		if ($dataCategory) {
			$request->session()->flash('massage', 'Berhasil Update Kategori!');
		}

		return redirect()->back();
	}

	public function delete($id)
	{
		$dataCategory = Category::withTrashed()->find($id);
		if (!$dataCategory->trashed()) {
			$dataCategory->delete();
		}else{
			$dataCategory->forceDelete();
		}
		return redirect()->back()->with(['message'=> 'Successfully deleted!!']);
	}
}
