<?php

namespace App\Http\Controllers\API\auth;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException; 
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Exception;
use App\User;

class RegisterController extends Controller
{
	public function register(Request $request)
	{
		try {
			$this->validate($request, [
				'name' => ['required', 'min:3'],
				'email'=> ['required','email', 'unique:users,email'],
				'password' => ['required', 'min:8', 'confirmed'],
			]);

			$user = User::create([
				'name' => $request->name,
				'email'=> $request->email,
				'password'=>bcrypt($request->password)
			]);
			
			$response['user'] = $user;
			$response['token'] = $user->createToken('myApp')->accessToken;

			$code = 200;
		} catch (Exception $e) {
			if ($e instanceof ValidationException) {
				$code = 404; // kalau data tidak ada
				$response = $e->errors();
			} else {
				$code=500;
				$response = $e->getMessage();
			}

		}
		return apiResponseBulider($code, $response);
	}

}
