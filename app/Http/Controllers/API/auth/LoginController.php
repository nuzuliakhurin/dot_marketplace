<?php

namespace App\Http\Controllers\API\auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB; 
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Auth;
use Exception;

class LoginController extends Controller
{
    public function login(Request $request)
    {
    	try {
            //Validasi inputan
    		$this->validate($request, [
    			'email'=>['required', 'email'],
    			'password'=>['required']
    		]);

            //pengecekan data untuk login

    		if (Auth::attempt([
    			'email'=>$request->email,
    			'password'=>$request->password
    		])) {
    			$user = Auth::user();
    			$response['user'] = $user;
    			$response['token'] = $user->createToken('myApp')->accessToken;
    			$code = 200;
    		}
    	} catch (Exception $e) {
    		if ($e instanceof ValidationException) {
				$code = 404; // kalau data tidak ada
				$response = $e->errors();
			} else {
				$code=500;
				$response = $e->getMessage();
			}
       }
       return apiResponseBulider($code, $response);
   }

   public function logout()
   {
       Auth::logout();
       return apiResponseBulider(200, 'Sukses Logout')
   }
}
