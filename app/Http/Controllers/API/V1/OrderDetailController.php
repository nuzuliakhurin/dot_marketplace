<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException; 
use Illuminate\Validation\ValidationException;
use Session;
use Exception;
use App\Products;
use App\OrderDetail;
use App\Orders;

class OrderDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     try {

        $this->validate($request, [
            'order_id'=>'required',
            'product_id' => 'required',
            'quantity' => 'required',

        ]);
            //ambil data order dengan id produk
        $checkProduct = OrderDetail::where('product_id', $request->product_id)->where('order_id', $request->order_id)->first();

        //ambil data harga produk
        $dataProducts = Products::where('id', $request->product_id)->first();

        //cek produk di detail ada atau tidak
        if ($checkProduct) {
            $checkProduct->order_id = $checkProduct->order_id;
            $checkProduct->quantity = $checkProduct->quantity + $request->quantity;
            //$checkProduct->price = $checkProduct->price;
            $checkProduct->price = $checkProduct->price + ($request->quantity * $dataProducts->unit_price);
            $checkProduct->save();
        } else {
            $dataOrderDetail = new OrderDetail;
            $dataOrderDetail->order_id      = $request->order_id;
            $dataOrderDetail->product_id    = $request->product_id;
            $dataOrderDetail->quantity      = $request->quantity;
            // $dataOrderDetail->price         = $request->price;
            $dataOrderDetail->price         = $request->quantity * $dataProducts->unit_price;
            $dataOrderDetail->save();
        }

        $getOrder = OrderDetail::where('order_id', $request->order_id)->get();

        $total = $getOrder->sum('price');

        $dataOrders = Orders::find($request->order_id);
        $dataOrders->total = $total;
        $dataOrders->save();

        $code = 200;
        $response = $dataOrders;
    } catch (Exception $e) {
        if ($e instanceof ValidationException) {
            $code = 400;
            $response = $e->errors();
        } else {
            $code=500;
            $response = $e->getMessage();
        }
    }
        return apiResponseBulider($code, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
