<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\ProductsResource;
use Illuminate\Database\Eloquent\ModelNotFoundException; 
use Illuminate\Validation\ValidationException;
use Session;
use Exception;
use App\Products;
use App\Category;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data = ProductsResource::collection(Products::paginate(3));
            $code = 200;
            $response = $data;
        } catch (Exception $e) {
            $code = 500;
            $response = $e->getMessage();
        }
        return apiResponseBulider($code, $response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $this->validate($request, [
                'name' => 'required',
                'category_id' => 'required',
                'unit_price' => 'required| numeric',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            Category::where('id', $request->category_id)->increment('product_count');
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);

            $dataProducts = new Products();

            $dataProducts->name = $request->name;
            $dataProducts->category_id = $request->category_id;
            $dataProducts->unit_price = $request->unit_price;
            $dataProducts->image = $imageName;
            
            $dataProducts->save();

            $code=200;
            $response = new ProductsResource($dataProducts);
            
        } catch (Exception $e) {
            if ($e instanceof ValidationException) {
                $code = 400;
                $response = $e->errors();
            } else {
                $code=500;
                $response = $e->getMessage();
            }
        }
        return apiResponseBulider($code, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $data = new ProductsResource(Products::findOrFail($id)) ;
            $code = 200;
            $response = $data;
        } catch (Exception $e) {
            $code = 500;
            $response = $e->getMessage();
        }
        return apiResponseBulider($code, $response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
                'category_id' => 'required',
                'unit_price' => 'required| numeric',
            ]);

            $dataProducts = Products::find($id);

            $dataProducts->name = $request->name;
            $dataProducts->category_id = $request->category_id;
            $dataProducts->unit_price = $request->unit_price;

            if($request->image) {

                $imageName = time().'.'.request()->image->getClientOriginalExtension();
                request()->image->move(public_path('/images'), $imageName);

                $dataProducts->image = $imageName;
            }

            $dataProducts->save();
            
            $dataProducts = new ProductsResource(Products::findOrFail($id)) ;
            $code = 200;
            $response = $dataProducts;
        } catch (Exception $e) {
          if ($e instanceof ValidationException) {
                $code = 400;
                $response = $e->errors();
            } else {
                $code=500;
                $response = $e->getMessage();
            }
       }
       return apiResponseBulider($code, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
