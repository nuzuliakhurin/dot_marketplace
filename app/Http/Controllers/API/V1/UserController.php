<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function detail() //Show detail user
    {
    	try {
    		$user = Auth::User();
    		$code = 200;
    		$response = $user;
    	} catch (Exception $e) {
    		if ($e instanceof ModelNotFoundException) {
				$code = 404; // kalau data tidak ada
				$response = 'Data Not Found';
			} else {
				$code=500;
				$response = $e->getMessage();
			}
    	}
    	return apiResponseBulider($code, $response);
    }
}
