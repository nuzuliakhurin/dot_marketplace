<?php 

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
/**
 * 
 */
class CobaController extends Controller
{
	public function method_get()
	{
		$data = [
			'nama' => 'Nuzulia',
			'umur' => 17,
			'asal' => 'Malang',
			'sekolah' => [
				'SDI Nurul Izzah',
				'SMPN 21 Malang',
				'SMK TELKOM Malang'
			]
		];
		return apiResponseBulider(200, $data);
	}

	public function method_post(Request $request)
	{
		$data = $request->all();
		if ($request->type == 'siswa') {
			$data['message'] = "Selamat datang siswa";
		} else {
			$data['message'] = "Selamat datang guru";
		}
		return apiResponseBulider(200, $data);
	}

	//Try Catch untuk menghandle error 

	public function method_cuoba(Request $request)
	{
		try{
			$data = $request->all();
			//buat kondisi yang akan discek, 1 = true, 2 = false
			if (($request->type == 'guru') && ($request->type == 'siswa')) throw new Exception("Type salah!", 1);
			if ($request->nama) throw new Exception("Nama Harus diisi", 1); 
			// if(!isset apiResponseBulider(200, $data));
			//return $this->apiResponseBulider(200, $data);
			$response = $data;
			$code = 200;
		}
	 catch(Exception $e){
	 	$code = 500;
	 	$response = $e->getMessage();
		//return apiResponseBulider(500, $e->getMessage());
	}
}
}
?>