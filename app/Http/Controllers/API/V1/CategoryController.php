<?php 

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException; 
use Illuminate\Validation\ValidationException;
use Session;
use Exception;
use App\Category;

/**
 * 
 */
class CategoryController extends Controller
{
	
	public function index()
	{
		try {
			$dataCategory = Category::paginate(3);
			$code=200;
			$response = $dataCategory;
			// return response()->json(categories);
		} catch (Exception $e) {
			$code = 500;
			$response = $e->getMessage();
			//return response("An error occured", 500);
		}
		return apiResponseBulider($code, $response);
	}

	public function show($id)
	{
		
		try {
			$dataCategory = Category::findOrFail($id); //menemukan atau tidak
			$code=200;
			$response = $dataCategory;
		} catch (Exception $e) {
			if ($e instanceof ModelNotFoundException) {
				$code = 404; // kalau data tidak ada
				$response = 'Data Not Found';
			} else {
				$code=500;
				$response = $e->getMessage();
			}
		}
		return apiResponseBulider($code, $response);
	}

	public function update(Request $request, $id)
	{
		try {
			//validasi
			$this->validate($request, [
				'category_name' => 'required',
			]);

			$dataCategory = Category::find($id);

			$dataCategory->category_name = $request->category_name;
			
			$dataCategory->save();

			$code = 200;
			$response = $dataCategory;
		} catch (Exception $e) {
			// $code = 500;
			// $response = $e->getMessage();
			if ($e instanceof ValidationException) {
				$code = 400;
				$response = $e->errors();
			} else {
				$code=500;
				$response = $e->getMessage();
			}
		}
		return apiResponseBulider($code, $response);
	}

	public function store(Request $request)
	{
		
		try {

			$this->validate($request, [
				'category_name' => 'required',
			]);

			$dataCategory = new Category;
			
			$dataCategory->category_name = $request->category_name;
			$dataCategory->product_count = 0;

			$dataCategory->save();
			$code=200;
			$response = $dataCategory;
			
		} catch (Exception $e) {
			if ($e instanceof ValidationException) {
				$code = 400;
				$response = $e->errors();
			} else {
				$code=500;
				$response = $e->getMessage();
			}
		}
		return apiResponseBulider($code, $response);
	}

	public function destroy($id)
	{
		try {
			$dataCategory = Category::find($id);
			$dataCategory->delete();
			return response("Sukses hapus"); 

		} catch (Exception $e) {
			return response("An error occured", 500);
		}
	}
}

?>