<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException; 
use Illuminate\Validation\ValidationException;
use App\Http\Controllers\Controller;
use App\Customers;
use Exception;

class CustomersController extends Controller
{
	public function index()
	{
		try {
			$dataCustomers = Customers::paginate(3);
			$code=200;
			$response = $dataCustomers;
		} catch (Exception $e) {
			$code = 500;
			$response = $e->getMessage();
		}
		return apiResponseBulider($code, $response);
	}

	public function show($id)
	{
		
		try {

			$dataCustomers = Customers::findOrFail($id);
			$code=200;
			$response = $dataCustomers;
		} catch (Exception $e) {
			if ($e instanceof ModelNotFoundException) {
				$code = 404; // kalau data tidak ada
				$response = 'Data Not Found';
			} else {
				$code=500;
				$response = $e->getMessage();
			}
		}
		return apiResponseBulider($code, $response);
	}

	public function update(Request $request, $id)
	{
		try {
			
			$this->validate($request, [
				'first_name' => 'required | min:3',
				'last_name' => 'required | min:3',
				'email' => 'required',
				'password' => 'required',
				'address' => 'required',
				'phone_number' => 'required',
			]);

			$dataCustomers = Customers::find($id);

			$dataCustomers->first_name = $request->first_name;
			$dataCustomers->last_name = $request->last_name;
			$dataCustomers->email = $request->email;
			$dataCustomers->password = $request->password;
			$dataCustomers->address = $request->address;
			$dataCustomers->phone_number = $request->phone_number;

			$dataCustomers->save();
			$code = 200;
			$response = $dataCustomers;

		} catch (Exception $e) {
			if ($e instanceof ValidationException) {
				$code = 400;
				$response = $e->errors();
			} else {
				$code=500;
				$response = $e->getMessage();
			}
		}
		return apiResponseBulider($code, $response);
	}

	public function store(Request $request)
	{
		
		try {		

			$this->validate($request, [
				'first_name' => 'required | min:3',
				'last_name' => 'required | min:3',
				'email' => 'required',
				'password' => 'required',
				'address' => 'required',
				'phone_number' => 'required',
			]);

			$dataCustomers = new Customers;

			$dataCustomers->first_name = $request->first_name;
			$dataCustomers->last_name = $request->last_name;
			$dataCustomers->email = $request->email;
			$dataCustomers->password = $request->password;
			$dataCustomers->address = $request->address;
			$dataCustomers->phone_number = $request->phone_number;

			$dataCustomers->save();
			$code=200;
			$response = $dataCustomers;
			
		} catch (Exception $e) {
			if ($e instanceof ValidationException) {
				$code = 400;
				$response = $e->errors();
			} else {
				$code=500;
				$response = $e->getMessage();
			}
		}
		return apiResponseBulider($code, $response);
	}

	public function destroy($id)
	{
		try {
			$dataCustomers = Customers::find($id);
			$dataCustomers->delete();
			return response("Sukses hapus data"); 

		} catch (Exception $e) {
			return response("An error occured", 500);
		}
	}
}
