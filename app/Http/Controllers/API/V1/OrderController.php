<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException; 
use Illuminate\Validation\ValidationException;
use Session;
use Exception;
use App\Orders;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
         $this->validate($request, [
            'customer_id'=>'required',
        ]);

         $dataOrder = new Orders;

         $dataOrder->customer_id = $request->customer_id;
         $dataOrder->total = 0;
         $dataOrder->save();

         $code = 200;
         $response = $dataOrder;

     } catch (Exception $e) {
        if ($e instanceof ValidationException) {
            $code = 400;
            $response = $e->errors();
        } else {
            $code=500;
            $response = $e->getMessage();
        }
    }
    return apiResponseBulider($code, $response);
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $dataOrder = Orders::findOrFail($id);
            $code = 200;
            $response = $dataOrder;
        } catch (Exception $e) {
            if ($e instanceof ModelNotFoundException) {
                $code = 404; // kalau data tidak ada
                $response = 'Data Not Found';
            } else {
                $code=500;
                $response = $e->getMessage();
            }
        }
        return apiResponseBulider($code, $response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
