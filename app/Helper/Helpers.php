<?php 


	 function apiResponseBulider($code, $data){
		if ($code == 200) {
			$response['status'] = 200;
			$response['data'] = $data;
		} elseif ($code == 400) {
			$response['status'] = 400;
			$response['message'] = $data;
		} elseif ($code == 404){
			$response['status'] = 404;
			$response['message'] = $data;
		} else {
			$response['status'] = 500;
			$response['message'] = $data;	
		}

		return response()->json($response, $code);
	} 
