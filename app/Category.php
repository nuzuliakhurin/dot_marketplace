<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = ['category_name'];

    use SoftDeletes;

    // tambah relasi oneto many

    public function products()
    {
    	return $this->hasMany(Products::class, 'category_id', 'id');
    }
    public function delete()
    	{
    		$this->products()->delete();
    		return parent::delete();
    	}
}