<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model
{
    protected $table = 'orders';
    use SoftDeletes;

    protected $fillable = ['customer_id', 'total'];

    public function customers()
    {
    	return $this->hasOne(Customers::class, 'id', 'customer_id');
    }

    public function orderDetails()
    {
    	return $this->hasMany(OrderDetail::class, 'order_id', 'id');
    }

    public function delete()
    {
    	$this->orderDetails()->delete();
    	return parent::delete();
    }
}
