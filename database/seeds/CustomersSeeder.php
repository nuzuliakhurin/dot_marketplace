<?php

use Illuminate\Database\Seeder;

use App\Customers;

class CustomersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Customers::create(
        // 	[
        // 		'email'=>'lala@lala.com',
        // 		'first_name'=>'lalae',
        // 		'last_name'=>'elala',
        // 		'address'=>'malang',
        // 		'phone_number'=>'678643',
        // 		'password'=> 'qqq'
        // 	],
        // 	[
        // 		'email'=>'bubu@bubu.com',
        // 		'first_name'=>'bubu',
        // 		'last_name'=>'bbibi',
        // 		'address'=>'malang',
        // 		'phone_number'=>'678643',
        // 		'password'=> 'ooo'
        // 	]
        // );

        factory(Customers::class, 2)->create();
    }
}
