<?php

use Illuminate\Database\Seeder;
use App\Products;
use App\Category;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//get random kategori
    	$category = Category::first();

    	//insert product
        // Products::create([
        // 	'category_id'=>$category->id,
        // 	'name'=>'Beras',
        // 	'unit_price'=>'20000']);

        factory(Products::class,2)->create(
        	[
        		'category_id' => $category->id
        	]
        );
    }
}
