<?php

use Illuminate\Database\Seeder;
use App\Category; //use model

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory(Category::class,2)->create();
        // Category::create(['category_name'=>'category 1']);
    }
}
