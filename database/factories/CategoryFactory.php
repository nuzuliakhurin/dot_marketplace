<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Category; //nama model
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'category_name'=>$faker->word,
        'product_count'=>$faker->randomDigit()
    ];
});
